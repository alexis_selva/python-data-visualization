These are the programming assignments relative to Python for everybody - capstone (5th part)

- Week 2: Building a Search Engine
- Week 4: Spidering and Modeling Email Data
- Week 6: Visualizing Email Data

For more information, I invite you to have a look at https://www.coursera.org/learn/python-data-visualization
