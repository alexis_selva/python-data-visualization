Mailing List Data - Part II
In this assignment you will visualize the mailing list data you have downloaded from http://mbox.dr-chuck.net/ and take some screen shots. Important: You do not have to download all of the data - it is completely acceptable to visualize a small subset of the data for this assignment.

Don't take off points for little mistakes. If they seem to have done the assignment give them full credit. Feel free to make suggestions if there are small mistakes. Please keep your comments positive and useful. 
Sample solution: http://www.py4e.com/code3/gmane.zip
